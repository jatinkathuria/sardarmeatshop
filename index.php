
<?php include 'include/header.php';?>

<body class="home">



<!--[if lt IE 8]>

<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

<![endif]-->



<div class="csi-container ">

    <!-- ***  ADD YOUR SITE CONTENT HERE *** -->

<?php include 'include/menu.php';?>

    <!--Banner-->

    <section>

        <div class="csi-banner csi-banner-typed">

            <div class="csi-banner-style">

                <div class="csi-inner">

                    <div class="container">

                        <div class="csi-banner-content">

                            <h3 class="csi-subtitle">Hot & Spicy</h3>

                            <h2 class="csi-title"><span id="csi-typed-string">Delicious Food</span></h2>

                            <div class="btn-area">

                                <a class="csi-btn" href="#">Contact Us</a>

                                <a class="csi-btn" href="#">Our Menu</a>

                            </div>

                        </div>

                    </div>

                    <!-- //.container -->

                </div>

                <!-- //.INNER -->

            </div>

        </div>

    </section>

    <!--Banner END-->



    <!--ABOUT-->

    <section>

        <div id="csi-about" class="csi-about csi-about-zikzak">

            <div class="csi-inner">

                <div class="container mainWrapperHome">
 

                     <div class="know-licious-way">
                <h2>
                    Know more about us
                </h2>
                <ul>
                    <li>
                        Premium
                        <p>Produce</p>
                    </li>
                    <li>
                        World-class central
                        <p>production unit</p>
                    </li>
                    <li>
                        150 Quality
                        <p>Checks</p>
                    </li>
                    <li>
                        Delivered Fresh
                        <p>Everyday</p>
                    </li>
                    <li>
                        Extraordinary
                        <p>cooking</p>
                    </li>
                </ul> 
 
            </div>

                </div><!-- //.CONTAINER -->

            </div><!-- //.INNER -->

        </div>

    </section>

    <!--ABOUT END--> 



    <!--MENU ITEMS-->

    <section>

        <div id="csi-menu" class="csi-menu">

            <div class="csi-inner">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="csi-heading">

                                <h3 class="subtitle">Food Recipe Items</h3>

                                <h2 class="title">Menu Of The Day</h2>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-nav-pills-area">
                                <ul class="nav nav-pills csi-nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#home"><img src="assets/img/tab-menu1.png" alt=""> Chicken</a></li>
                                    <li><a data-toggle="pill" href="#menu1"><img src="assets/img/tab-menu2.png" alt=""> Mutton</a></li>
                                    <li><a data-toggle="pill" href="#menu2"><img src="assets/img/tab-menu3.png" alt=""> Pork</a></li>
                                    <li><a data-toggle="pill" href="#menu3"><img src="assets/img/tab-menu4.png" alt=""> Sea Food</a></li>
                                </ul>
                            </div>



                            <div class="tab-content csi-tab-content">
                                <div id="home" class="tab-pane fade in active">

                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="item-bord">
                                        <div class="item-thumb">
                                            <img src="assets/img/chicken_nug.jpg">
                                            <div class="disc-label show">20 %OFF</div>
                                        </div>
                                        <div class="item-details">
                                            <div class="item-title">
                                                Chicken Malai Tikka (Boneless)
                                            </div>
                                            <p class="item-weight"></p>
                                            <p class="item-desc">
                                                Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                            </p>
                                            <p class="item-weight show">
                                                <span class="net-weight">Net wt: 450gm 
                                                    <span class="info-icon hide">
                                                        <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                    </span>
                                                    <span class="info-bubble"></span>
                                                </span>
                                                <span class="gross-weight">
                                                    Pieces: 9 - 11
                                                </span>
                                            </p>
                                            <p class="item-weight hide">
                                                <span class="net-weight show">Net wt: 450gm
                                                    <span class="info-icon hide">
                                                        <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                    </span>
                                                    <span class="info-bubble"></span>
                                                </span>
                                                <span class="gross-weight">
                                                    Pieces: 9 - 11
                                                </span>
                                            </p>
                                            <div class="item-action">
                                                <div class="rate">
                                                    <span class="rupee ">239</span>
                                                    <span class="rupee disc show">299</span>
                                                </div>
                                                <div class="action">
                                                    <div class="action-slider show">
                                                        <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                                        <p>
                                                            <span class="cart-btns remove-one" ></span>
                                                            <span class="item-qty">0</span>
                                                            <span class="cart-btns add-one"></span>
                                                        </p>
                                                        <p>Out of stock</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    </div> 

                                </div>

                                <div id="menu1" class="tab-pane fade">

                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                                <div class="disc-label show">20 %OFF</div>
                                            </div>
                                            <div class="item-details">
                                                <div class="item-title">
                                                    Chicken Malai Tikka (Boneless)
                                                </div>
                                                <p class="item-weight"></p>
                                                <p class="item-desc">
                                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                                </p>
                                                <p class="item-weight show">
                                                    <span class="net-weight">Net wt: 450gm 
                                                        <span class="info-icon hide">
                                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                        </span>
                                                        <span class="info-bubble"></span>
                                                    </span>
                                                    <span class="gross-weight">
                                                        Pieces: 9 - 11
                                                    </span>
                                                </p>
                                                <p class="item-weight hide">
                                                    <span class="net-weight show">Net wt: 450gm
                                                        <span class="info-icon hide">
                                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                        </span>
                                                        <span class="info-bubble"></span>
                                                    </span>
                                                    <span class="gross-weight">
                                                        Pieces: 9 - 11
                                                    </span>
                                                </p>
                                                <div class="item-action">
                                                    <div class="rate">
                                                        <span class="rupee ">239</span>
                                                        <span class="rupee disc show">299</span>
                                                    </div>
                                                    <div class="action">
                                                        <div class="action-slider show">
                                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                                            <p>
                                                                <span class="cart-btns remove-one" ></span>
                                                                <span class="item-qty">0</span>
                                                                <span class="cart-btns add-one"></span>
                                                            </p>
                                                            <p>Out of stock</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 

                                </div>

                                <div id="menu2" class="tab-pane fade">

                                   <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                                <div class="disc-label show">20 %OFF</div>
                                            </div>
                                            <div class="item-details">
                                                <div class="item-title">
                                                    Chicken Malai Tikka (Boneless)
                                                </div>
                                                <p class="item-weight"></p>
                                                <p class="item-desc">
                                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                                </p>
                                                <p class="item-weight show">
                                                    <span class="net-weight">Net wt: 450gm 
                                                        <span class="info-icon hide">
                                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                        </span>
                                                        <span class="info-bubble"></span>
                                                    </span>
                                                    <span class="gross-weight">
                                                        Pieces: 9 - 11
                                                    </span>
                                                </p>
                                                <p class="item-weight hide">
                                                    <span class="net-weight show">Net wt: 450gm
                                                        <span class="info-icon hide">
                                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                        </span>
                                                        <span class="info-bubble"></span>
                                                    </span>
                                                    <span class="gross-weight">
                                                        Pieces: 9 - 11
                                                    </span>
                                                </p>
                                                <div class="item-action">
                                                    <div class="rate">
                                                        <span class="rupee ">239</span>
                                                        <span class="rupee disc show">299</span>
                                                    </div>
                                                    <div class="action">
                                                        <div class="action-slider show">
                                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                                            <p>
                                                                <span class="cart-btns remove-one" ></span>
                                                                <span class="item-qty">0</span>
                                                                <span class="cart-btns add-one"></span>
                                                            </p>
                                                            <p>Out of stock</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>  

                                </div>

                                <div id="menu3" class="tab-pane fade">

                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>

                </div><!-- //.CONTAINER -->

            </div><!-- //.INNER -->

        </div>

    </section>

    <!--MENU ITEMS END--> 
    <section class="crousel_products">
        <div class="container">
            <span class="cp_heading">Featured Products</span>

            <div class="fp_ah_prolist">
                <div class="owl-carousel">
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!--FEEDBACK-->

    <section class="feedbackSect">
        <div id="csi-feedback" class="csi-feedback">
            <div class="container">
                <div class="csi-feedback-bg">
                    <div class="csi-testi-feedback">
                        <div class="feedback-inner-bg">
                            <div class="feedback-inner">
                                <div class="csi-heading">
                                    <h2>Our Client Feedbacks</h2>
                                </div>
                                <div id="csi-owltestimonial" class="owl-carousel csi-owltestimonial">
                                    <div class="item">
                                        <figure class="csi-client-image">
                                            <img src="assets/img/client1.jpg" alt="testpersion1"/>
                                        </figure>
                                        <div class="testi-info-area">
                                            <p class="csi-review">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                                            </p>
                                            <h4 class="csi-client-name">Jonathon Doe</h4>
                                        </div>
                                    </div> <!--//.Item-->
                                    <div class="item">
                                        <figure class="csi-client-image text-center">
                                            <img src="assets/img/client1.jpg" alt="testpersion1"/>
                                        </figure>
                                        <div class="testi-info-area">
                                            <p class="csi-review">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                                            </p>
                                            <h4 class="csi-client-name">Jonathon Roy</h4>
                                        </div>
                                    </div> <!--//.Item-->
                                    <div class="item">
                                        <figure class="csi-client-image">
                                            <img src="assets/img/client1.jpg" alt="testpersion1"/>
                                        </figure>
                                        <div class="testi-info-area">
                                            <p class="csi-review">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                                            </p>
                                            <h4 class="csi-client-name">Jonathon Islam</h4>
                                        </div>
                                    </div> <!--//.Item-->
                                </div><!--l//#csi-OWL TESTIMONIAL-->
                            </div>
                        </div>
                    </div> <!--//.Testimonials-->
                </div>
            </div>
        </div>
    </section>

    <section class="crousel_products">
        <div class="container">
            <span class="cp_heading">Recently Viewed Products</span>

            <div class="fp_ah_prolist">
                <div class="owl-carousel">
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p>Out of stock</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!--FEEDBACK END-->

<?php include 'include/footer.php';?>