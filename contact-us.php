<?php include 'include/header.php';?>
<body class="page page-template">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="csi-container ">
    <!-- ***  ADD YOUR SITE CONTENT HERE *** -->

    <!--HEADER-->
    <?php include 'include/menu.php';?>
    <!--HEADER END-->


    <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading-area">
                                <h2 class="csi-heading">
                                    Get In Touch / Contact
                                </h2>
                                <ul class="breadcrumb">
                                    <li><a href="index.html"><i class="icon-home6"></i>Home</a></li>
                                    <li class="active">Contact</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div>
                <!-- //.container -->
            </div>
        </div>
    </section>
    <!--Banner END-->


    <section>
        <div id="csi-contact" class="csi-contact">
            <div class="csi-inner">
                <div class="contact-top-area">
                    <div class="contact-top-left">
                        <div class="contact-info">
                            <div class="csi-box">
                                <span class="csi-icon"><i class="fa fa-headphones"></i></span>
                                <div class="address">
                                    <p>+2545-8546-XXX</p>
                                    <p>+2545-8546-XXX</p>
                                </div>
                            </div>
                            <div class="csi-box">
                                 
                                <div class="address">
                                    <p>Email:<a href="#" style="color:#fff">[email&#160;protected]</a></p>
                                    <p><a href="#" style="color:#fff">[email&#160;protected]</a></p>
                                </div>
                            </div>
                            <div class="csi-box">
                                <span class="csi-icon"><i class="fa fa-map-marker"></i></span>
                                <div class="address">
                                    <p>123 Grand Tower - 45 Street Name,
                                        City Name, United State</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contact-top-right">
                        <div class="innerpage-section">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" frameborder="0" style="width:100%;height:416px;border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <form method="POST" class="csi-contactform" action="http://httpcoder.com/demo/html/Sardar Meat Shop/view/php/form-handler.php">
                                <div class="form-group">
                                    <input type="text" name="csiname" class="form-control csiname" id="csiname" placeholder="Enter Your Name ..." required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="csiemail" class="form-control csiemail" id="csiemail" placeholder="Enter Email address ..." required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="csisubject" class="form-control csisubject" id="csisubject" placeholder="Enter Email Subject ..." required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control csimessage" name="csimessage" id="csimessage" rows="5" placeholder="Leave me A Massage ..." required></textarea>
                                </div>
                                <button type="submit" name="submit" value="contact-form" class="csi-btn hvr-glow hvr-radial-out csisend csi-send">Send Massage</button>
                            </form>
                            <!-- MODAL SECTION -->
                            <div id="csi-form-modal" class="modal fade csi-form-modal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content csi-modal-content">
                                        <div class="modal-header csi-modal-header">
                                            <button type="button" class="close brand-color-hover" data-dismiss="modal" aria-label="Close">
                                                <i class="fa fa-power-off"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert csi-form-msg" role="alert"></div>
                                        </div> <!--//MODAL BODY-->
                                    </div>
                                </div>
                            </div> <!-- //MODAL -->
                        </div> <!--//.COL-->
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
	
	<section>
        <div id="csi-news" class="csi-news">
            <div class="csi-inner">
                <div class="container">
				
                    <div class="row" style="    margin-bottom: 70px;">
                        <div class="col-sm-12 text-center">
                            <span class="csi-btn" href="news-list.html">ALL FRANCHIESE SHOP ADDRESS</span>
                        </div>
                    </div>
					
                    <div class="news-area">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">1</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">SARDAR ASHOK VIHAR</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 123 Grand Tower - 45 Street Name,
														City Name, United State
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:1165397230"> <span class="csi-icon"><i class="fa fa-mobile"></i></span> 1165397230</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">2</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">SARDAR MODEL TOWN</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														G-8SHOP-6 MODEL TOWN 3rd NEAR PUNJAB NATIONAL BANK
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9560044539"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9560044539
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">3</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">SARDAR RANI BAGH</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														1465,SHOP-02 RANI BAGH
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9910107686"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9910107686
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">4</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">SARDAR (ROHINI)</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														34,CSC,6 PARK PAZZAN MARKET ROHINI SEC-9DELHI-85
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9891182105"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9891182105
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">5</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">BINDRA ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														GH-14/977,PASCHIM VIHAR DELHI-110087
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9810528772"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9810528772
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">6</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">BINDRA ENTERPRISES B-2</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														JWALA HERI MARKET PASCHIM VIHAR DELHI
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9810528772"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9810528772
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">7</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">M/S H FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														B-2  GREATER KAILASH ENCLAVE2  SHOP NO C OPPOSITE DT CINEMAS DELHI
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9899994040"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9899994040
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">8</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">ANHAD FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO 164, DEFENCE COLONY FLYOVER MARKET,DELHI 110024
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811108886"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9811108886
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">9</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">A. S ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														5/46 SHANKAR ROAD NEAR CCD OLD RAJINDER NAGAR DELHI  PH.NO. 9910799195
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811163736"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9811163736
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">10</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">JINNY ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														GG-I/143A, PVR ROAD VIKAS PURI NEW DELHI
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9447240533"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9447240533
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">11</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">OMNI FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														UG- 13 PLOT NO. B223 PALAM EXTN, SECTER-07 DWARKA-110077
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9694176000"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9694176000
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">12</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">A&A FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														10/8 CHOTI SUBZI MANDI JANAK PURI DELHI-110058 M,NO. 09810292325
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9810292325"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9810292325
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">13</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">M/S M.R. FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														F-2, KIRTI NAGAR CENTRAL MKT DELHI PH.NO.011-65001231
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9716768367"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9716768367
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">14</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">DELECACY FOODS(M. VIHAR)</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														C-1 NAVEEN SHAHDRA DELHI- 110032 PH.NO.011-22321428
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9953636365"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9953636365
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">15</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">KOCHAR ASSOCIATES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO.16, SECTOR -15 MARKET FARIDABAD,HARYANA
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9899656100"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9899656100
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">16</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">AVNOOR ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO-5 GROUND FLOOR, ACHIVER MALL NEAR SAINIK COLONY SECTOR-49 FARIDABAD
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9999322655"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9999322655
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">17</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">RSNH ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														24,5D,NIT, FARIDABAD HARYANA PH, NO. 9873713974
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9873713974"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9873713974
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">18</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">THE MEAT STREET</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO. 152 VYAPAR KENDRA SUSHANT LOK -1 GURGAON
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811009390"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9811009390
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">19</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">THE MEAT STREET THE SAPPHIRE </a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO 146, 1ST FLOOR,SEC-49, SOHNA ROAD GURGAON
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9313978953"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9313978953
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">20</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">V W FOOD MART</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO. 102, HUDA MARKET, SECTOER 15 PART-2,GURGAON, HARYANA , INDIA GURGAON (WEST)
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811677795"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9811677795
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">21</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">KESHAR FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														B-18/3750/20A,UPPER FLOOR,DUGRI ROAD LUDHIAYANA,LUDHIAYANA, PUNJAB,141002
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811677795"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9811677795
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">22</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">GOBIND FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														1612,OUTRAM LINES KINGSWAY CAMP, GTB NAGAR-110009 PH, NO 9899283131
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811094243"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9811094243
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">23</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">PRIME MEAT SHOP</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO. 135, SECTOR-23 HUDA MARKET PALAM VIHAR, GURGAON , HARYANA
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811094243"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9871008259
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">24</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">MRG ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														E 46. GROUND FLOOR NARAINA VIHAR, DELHI -110028
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9811094243"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9999317838
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">25</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">PANCHHI'S</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														113/41 RAJOURI GARDEN, RAJUORI -110027,
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:7838717247"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													7838717247
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">26</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">SKT FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														GF-9 PARAMOUNT FLOOURENCEP PLAZA SEC 137 NOIDA - 201301
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:7838717247"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9810221529
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">27</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">K.S FOODS</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP.NO.2 PURUSHOTAM MARKET NEAR VISHAL  MEGA MART KUNJPURA ROAD KARNAL 
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:7838717247"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													7404546836
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">28</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">SGS INFINITY LLP</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOPNO.18,COMMERCIAL COMPLEX GARDENIA GLORY I QSQUARE,SECTOR-46 NOIDA
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:7838717247"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9582528893
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">29</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">N.P FROZEN FOODS </a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO.9 GROUND FLOOR BLOCK ANSAL FORTUNE ARCADE SECTOR 18 NOIDA
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9718002233"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9718002233
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="single-news">
                                    <figure>
                                        <a href="news-single.html">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112067.44588624254!2d77.14496737261379!3d28.62653454681471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcd57523a120dd8b4!2sSardar+A+Pure+Meat+Shop!5e0!3m2!1sen!2sin!4v1513030523392" width="200" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
										</a>
                                        <figcaption style="height: auto;width: auto;padding: 10px;">
                                            <h4 class="date">30</h4>
                                        </figcaption>
                                    </figure>
                                    <div class="news-content">
                                        <h2 class="title"><a href="news-single.html">D.J. ENTERPRISES</a></h2>
                                        <div>
											<div class="csi-box">
												<div class="address">
													<p style="padding-top: 15px;margin-bottom: 15px;"> 
														<span class="csi-icon"><i class="fa fa-map-marker"></i></span> 
														SHOP NO.30 Lu BLOCK DDA MARKET PITAMPURA DELHI-110034
													</p>
												</div>
												
												<a style="font-size: 26px;padding-top:15px" class="readmore" href="tel:9899166832"> 
													<span class="csi-icon"><i class="fa fa-mobile"></i></span> 
													9899166832
												</a>
											</div>
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div> <!--//.row-->
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.NEWS-->
    
<?php include 'include/footer.php';?>