<?php include 'include/header.php';?>

<body class="page page-template">

    
    <section class="login_sec">
        <div class="container">
            <div class="login_header">
                <div class="logo_setter text-left">
                    <img src="assets/img/logo.png">
                </div>
            </div>

            <div class="login_div_container  animated slideInDown">
                <div class="login_div">
                    <div class="displayinblock width100">
                        <div class="ld_dw_heading">Forget Password</div>
                        <div class="ld_dw_lform">
                            <div class="ldlf_form-control">
                                <i class="fa fa-phone"></i>
                                <lable>Enter Your Number</lable>
                                <input type="text">
                                <a class="rght-txt-vr">Send OTP</a>
                            </div>
                            <div class="ldlf_form-control">
                                <i class="fa fa-shield"></i>
                                <lable>Verify OTP</lable>
                                <input type="text">
                            </div>
                            <div class="ldlf_form-control">
                                <a href="#" class="signin_button">
                                    Reset Password
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 

    

<?php include 'include/footer.php';?>