
<div id="contentDiv" class="CT_Banner">
    <div class="CT_BannerInner"> 
        <div class="container text-center" >
            <div class="title displayinblock">
                Get FLAT&nbsp;<b>20% OFF</b> on your FIRST order. Use Code:
            </div>
            <a class="btna displayinblock" target="_blank">NEW20</a>
        </div>

        <p class="para_cross">
            <span class="CT_BannerClose" id="CT_BannerClose" onclick="removeMaindiv()">x</span>
            
            <script>
                function removeMaindiv() {
                    var mx_theme=document.getElementById("contentDiv");
                    mx_theme.remove();
                }
            </script>
        </p>
    </div>
</div>
<div class="top-header">
    <div class="width100 th-container">
        <div class="container">
            <div class="float-left th-content-taker text-left col-md-6">
                <ul class=" tct-links list-inline ot-ct">
                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> Openning Hours:  8.00AM - 18.00PM</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> Call Us:   (+1) 555 234-8765 </li>
                </ul>
            </div>
            <div class="float-right th-content-taker text-right col-md-6">
                <a href="login.php" class="tct-links">Login</a> | <a href="signup.php" class="tct-links">Register</a>
            </div>
        </div>
    </div>
</div>
    <!--HEADER-->
    <header>
        <div id="csi-header" class="csi-header csi-banner-header csi-header-zikzak">
            <div class="header-top">
                <div class="header-top-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="csi-logo">
                                    <a href="index.php">
                                        <img src="assets/img/logo.png" alt="Logo"/>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="contact loc-setter">
                                    <ul class="list-inline">
                                        <li>
                                            Showing availability for   
                                            
                                            <a href="#" class="location-name" title="Sector 11, Gurugram, Haryana, India"  data-toggle="modal" data-target="#select_location">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                Sector 11, Gurugram
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="right-menu">
                                    <div class="collapse navbar-collapse">
                                        <ul class="nav navbar-nav csi-nav">
                                            
                                            <li>
                                                <a class="csi-btn" href="#">Cart</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--//.header-top--> 
            <div class="header-new">
                <div class="cat-list">
                    <div class="container">
                        <ul class="parent">
                            <li class="">
                                <a class="active">Chicken</a>
                            </li>
                            <li class="">
                                <a class="active">Cold Cuts</a>
                            </li>
                            <li class="">
                                <a  class="active">Today's Deals</a>
                            </li>
                            <li class="">
                                <a class="active">Lamb &amp; Goat</a>
                            </li>
                            <li class="">
                                <a class="active">Fish &amp; Seafood</a>
                            </li>
                            <li class="">
                                <a class="active hover">Marinades</a>
                            </li>
                            <li class="">
                                <a class="active">Spreads &amp; Pickles</a>
                            </li>
                            <li class="">
                                <a class="active">Eggs</a>
                            </li>
                            <li class="">
                                <a  class="active">Exotic Meats</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- //.INNER-->
        </div>
    </header>
    <!--HEADER END-->




<!-- Modal -->
<div id="select_location" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><center>Choose delivery location</center></h4>
      </div>
      <div class="modal-body">
        <div class="location_setter_pup">
            <div class="location_setter_pup_inner">
                <i class="fa fa-search" aria-hidden="true"></i>
                <input type="text" class="lspi_box" placeholder="Enter your location..">
                <a href="#" class="lspia_boxa"><i class="fa fa-location-arrow" aria-hidden="true"></i> Use my location</a>
            </div>
        </div>
      </div>
    </div>

  </div>
</div>