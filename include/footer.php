<footer>
        <div id="csi-footer" class="csi-footer">
            <div class="csi-inner">
                <div class="footer-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="footer-top">
                                    <a class="footer-logo" href="index.html"><img src="assets/img/logo.png" alt="logo"/></a>
                                </div>
                            </div>
                        </div><!--//.ROW-->
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                <div class="single">
                                    <h3 class="title">Contact</h3>
                                    <p>345 Park Ave, San Jose, CA 95110, United States</p>
                                    <p><a href="#" class="__cf_email__">sardarpuremeatshop@gmail.com</a></p>
                                </div>
                            </div> <!--//.col-->
                            <div class="col-xs-12 col-sm-3">
                                <div class="single">
                                    <h3 class="title">Links</h3>
                                    <ul>
                                        <li>
                                            <a href="#">Franchise Details</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us</a>
                                        </li>
                                        <li>
                                            <a href="#">About Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </div> <!--//.col-->
                            <div class="col-xs-12 col-sm-3">
                                <div class="single">
                                    <h3 class="title">Opening Time</h3>
                                    <p>Mon - Thu 11:30 - 22:00 clock </p>
                                    <p>Fri - Sat 11:30 - 24:00 clock </p>
                                </div>
                            </div> <!--//.col-->
                            <div class="col-xs-12 col-sm-3">
                                <div class="single">
                                    <h3 class="title">Social Links</h3>
                                    <ul class="list-inline footer-social">
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div> <!--//.col-->
                        </div> <!--//.main row-->
                    </div><!-- //.CONTAINER -->
                </div>
                <div class="csi-footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>Sardar Meat Shop | &copy; 2018 All Rights Reserved | Maintained by <a href="http://webmagnificent.com/" target=_blank>WebMagnificent</a></p>
                            </div>
                        </div><!--//.ROW-->
                    </div><!-- //.CONTAINER -->
                </div><!-- //.INNER-->
            </div>
        </div>
    </footer>

</div> <!--//.csi SITE CONTAINER-->
<!-- *** ADD YOUR SITE SCRIPT HERE *** -->
<!-- JQUERY  -->
<script data-cfasync="false" src="../../../../cdn-cgi/scripts/ddc5a536/cloudflare-static/email-decode.min.js"></script><script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
<!-- BOOTSTRAP JS  -->
<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>
<!-- bootstrap date picker js with moment js -->
<script src="assets/libs/datepicker/moment-with-locales.min.js"></script>
<script src="assets/libs/datepicker/bootstrap-datetimepicker.min.js"></script>
<!-- SKILLS SCRIPT  -->
<script src="assets/libs/jquery.validate.js"></script>
<!-- if load google maps then load this api, change api key as it may expire for limit cross as this is provided with any theme -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzfNvH2kifJQ0PhBIyuuG-swdkW1NPQVE"></script>
<!-- CUSTOM GOOGLE MAP -->
<script type="text/javascript" src="assets/libs/gmap/jquery.googlemap.js"></script>
<!-- Owl Carousel  -->
<script src="assets/libs/owlcarousel/owl.carousel.min.js"></script>
<!-- tweetie feed js  -->
<script src="tweetie/tweetie.js"></script>
<!-- adding magnific popup js library -->
<script type="text/javascript" src="assets/libs/maginificpopup/jquery.magnific-popup.min.js"></script>
<!-- type js -->
<script src="assets/libs/typed/typed.min.js"></script>
<!-- SMOTH SCROLL -->
<script src="assets/libs/jquery.smooth-scroll.min.js"></script>
<script src="assets/libs/jquery.easing.min.js"></script>
<!-- Counter JS -->
<script src="assets/libs/waypoints.min.js"></script>
<script src="assets/libs/counterup/jquery.counterup.min.js"></script>
<!-- CUSTOM SCRIPT  -->
<script src="assets/js/custom.script.js"></script>
</body>
</html>