<?php include 'include/header.php';?>

<body class="page page-template">  
<div class="csi-container ">
<?php include 'include/menu.php';?>
    <!--Banner-->

    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading-area">
                                <h2 class="csi-heading">
                                    Chicken Category
                                </h2>
                                <ul class="breadcrumb">
                                    <li><a href="index.html"><i class="icon-home6"></i>Home</a></li>
                                    <li class="active">Chicken</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div>
                <!-- //.container -->
            </div>
        </div>
    </section>
    <section>
        <div id="csi-menu" class="csi-menu csi-menu-inner">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">  
                            <div class="tab-content csi-tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                                <div class="disc-label show">20 %OFF</div>
                                            </div>
                                            <div class="item-details">
                                                <div class="item-title">
                                                    Chicken Malai Tikka (Boneless)
                                                </div>
                                                <p class="item-weight"></p>
                                                <p class="item-desc">
                                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                                </p>
                                                <p class="item-weight show">
                                                    <span class="net-weight">Net wt: 450gm 
                                                        <span class="info-icon hide">
                                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                        </span>
                                                        <span class="info-bubble"></span>
                                                    </span>
                                                    <span class="gross-weight">
                                                        Pieces: 9 - 11
                                                    </span>
                                                </p>
                                                <p class="item-weight hide">
                                                    <span class="net-weight show">Net wt: 450gm
                                                        <span class="info-icon hide">
                                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                                        </span>
                                                        <span class="info-bubble"></span>
                                                    </span>
                                                    <span class="gross-weight">
                                                        Pieces: 9 - 11
                                                    </span>
                                                </p>
                                                <div class="item-action">
                                                    <div class="rate">
                                                        <span class="rupee ">239</span>
                                                        <span class="rupee disc show">299</span>
                                                    </div>
                                                    <div class="action">
                                                        <div class="action-slider show">
                                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                                            <p>
                                                                <span class="cart-btns remove-one"></span>
                                                                <span class="item-qty-pdp">0</span>
                                                                <span class="cart-btns add-one"></span>
                                                            </p>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <section class="crousel_products">
        <div class="container">
            <span class="cp_heading">Featured Products</span>

            <div class="fp_ah_prolist">
                <div class="owl-carousel">
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div> 
                        <div class="item-bord">
                            <div class="item-thumb">
                                <img src="assets/img/chicken_nug.jpg">
                                <div class="disc-label show">20 %OFF</div>
                            </div>
                            <div class="item-details">
                                <div class="item-title">
                                    Chicken Malai Tikka (Boneless)
                                </div>
                                <p class="item-weight"></p>
                                <p class="item-desc">
                                    Boneless chicken in homemade cream with a hint of green chilli for a rich, flavourful bite
                                </p>
                                <p class="item-weight show">
                                    <span class="net-weight">Net wt: 450gm 
                                        <span class="info-icon hide">
                                            <img class="" src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <p class="item-weight hide">
                                    <span class="net-weight show">Net wt: 450gm
                                        <span class="info-icon hide">
                                            <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/info-dark.png">
                                        </span>
                                        <span class="info-bubble"></span>
                                    </span>
                                    <span class="gross-weight">
                                        Pieces: 9 - 11
                                    </span>
                                </p>
                                <div class="item-action">
                                    <div class="rate">
                                        <span class="rupee ">239</span>
                                        <span class="rupee disc show">299</span>
                                    </div>
                                    <div class="action">
                                        <div class="action-slider show">
                                            <button class="add-to-cart addCartBtn-home">Add To Cart</button>
                                            <p>
                                                <span class="cart-btns remove-one" ></span>
                                                <span class="item-qty">0</span>
                                                <span class="cart-btns add-one"></span>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </section>
    <!--MENU ITEMS END-->
<?php include 'include/footer.php';?>

