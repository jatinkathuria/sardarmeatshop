<?php include 'include/header.php';?>

<body class="page page-template">



<!--[if lt IE 8]>

<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

<![endif]-->



<div class="csi-container ">

    <!-- ***  ADD YOUR SITE CONTENT HERE *** -->

<?php include 'include/menu.php';?>

    <!--Banner-->

    <section>

        <div class="csi-banner csi-banner-inner" style="background-image:url('assets/product-images/pr_img_59a6d187ee369.jpg');
     background-position: center;">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading-area">
                                <h2 class="csi-heading">
                                    Fish Tikka (Boneless)
                                </h2>
                                <ul class="breadcrumb">
                                    <li><a href="index.html"><i class="icon-home6"></i>Home</a></li>
                                    <li class="">Menu</li>
                                    <li class="active">Fish Tikka (Boneless)</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div>
                <!-- //.container -->
            </div>
        </div>
    </section>

    <!--Banner END-->

    <section>
        <div class="sp-sec-det">
            <div class="container">
                <div class="displayinblock width100">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="img-ssd-dw-cc">
                                <img src="assets/product-images/pr_img_59a6d187ee369.jpg">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class='cm-cx-details'>
                                <div class="product-desc-block" data-prod="pr_6j4jhlr52f4">
                                    <p class="title">
                                        <span class="heading">Lucknowi Keema (Goat)</span>
                                        <span class="sub-heading1"></span>
                                    </p>
                                    <div class="sub-title">
                                    <span class="sub-title-list">Goat Keema</span>
                                    <span class="sub-title-list"> Keema</span></div>
                                    <div class="product-desc">
                                        <p><span>Premium fat-rich, wastage-free goat mince crafted in the traditions of the royal Awadhi kitchen to guarantee you the authentic keeema experience. With the complex and subtle balance of dry heat and fresh, ground flavours this meaty-mouthful will be a definite sweat-breaker and a proven crowd-pleaser.</span></p>
                                    </div>

                                    <div class="quantity-block">
                                        <div class="qty-block">
                                            <div class="block-container">
                                                <div class="block1">
                                                    <div class="icon-image">
                                                        <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/Pieces.png">
                                                    </div>
                                                    <div class="block-text UL">No. of Pieces NA</div>
                                                </div>
                                                <div class="block2">
                                                    <div class="icon-image">
                                                        <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/Serves.png">
                                                    </div>
                                                    <div class="block-text UR">Serves 2</div>
                                                </div>
                                            </div>

                                            <div class="block-container">
                                                <div class="block1">
                                                    <div class="icon-image">
                                                      <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/Time.png">
                                                    </div>
                                                    <div class="block-text LL">Cooking time 12-15 mins</div>
                                                </div>

                                                <div class="block2">
                                                    <div class="icon-image">
                                                      <img src="https://d2407na1z3fc0t.cloudfront.net/Banner/Netwt.png">
                                                    </div>
                                                    <div class="block-text LR">Net wt. 300gm</div>
                                                </div>
                                            </div>
                                            </div>
                                    </div>


                                    <div class="rate-block">
                                        <div class="rate">
                                            <span class="rate1"></span>
                                            <span class="rate2">₹&nbsp;249/-</span>
                                        </div>

                                        <div class="action">
                                            <span class="out-stock" style="display: inline;">Out of Stock</span>
                                            <div class="action-slider1">
                                                <button class="add-to-cart addCartBtn-pdp" data-cat="marinades" data-amt="249" data-text="Lucknowi Keema (Goat)" data-prid="pr_6j4jhlr52f4" data-qty="1">Add To Cart</button>
                                                <p>
                                                    <span class="cart-btns remove-one"></span>
                                                    <span class="item-qty-pdp">0</span>
                                                    <span class="cart-btns add-one" data-cat="marinades" data-amt="249" data-prid="pr_6j4jhlr52f4" data-text="Lucknowi Keema (Goat)" data-qty="1"></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tag_line">
                                        <p>Available in <b>90 minutes</b> and for <b>Scheduled Delivery</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="sardariousWay"> 
        <div class="licious-way">
            <div class="container">
                <div class="wrapper">
                    <div class="licious-way-title">
                        <span class="full-word">
                            Our <span class="only-licious">Process</span>
                        </span>
                        <div class="title-border">
                        </div>
                    </div>
                    <ul class="way-image-list">
                        <li class="contain-block">
                            <div class="way-image">
                                <img src="assets/img/USP1.png" style="width: 80%;">
                            </div>
                            <div class="way-desc">
                                <span>Premium produce, sourced directly from the origin</span>
                            </div>
                        </li>
                        <li class="contain-block">
                            <div class="way-image">
                                <img src="assets/img/USP2.png" style="width: 100%;">
                            </div>
                            <div class="way-desc">
                                <span>Scientifically designed central production Unit</span>
                            </div>
                        </li>
                        <li class="contain-block">
                            <div class="way-image">
                                <img src="assets/img/USP3.png" style="width: 74%;">
                            </div>
                            <div class="way-desc">
                                <span>Compliance to stringent quality checks</span>
                            </div>
                        </li>
                        <li class="contain-block">
                            <div class="way-image">
                                <img src="assets/img/USP4.png" style="width: 59%;">
                            </div>
                            <div class="way-desc">
                                <span>Delivered fresh everyday</span>
                            </div>
                        </li>
                        <li class="contain-block">
                            <div class="way-image">
                                <img src="assets/img/USP5.png" style="width: 58%;">
                            </div>
                            <div class="way-desc">
                                <span>Experience extraordinary cooking</span>
                            </div>
                        </li>
                    </ul>
                    
                </div>
            </div>
        </div>
    </section>
    <!--MENU ITEMS END-->



<?php include 'include/footer.php';?>
