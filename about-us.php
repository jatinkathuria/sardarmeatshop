<?php include 'include/header.php';?>
<body class="page page-template">

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="csi-container ">
    <!-- ***  ADD YOUR SITE CONTENT HERE *** -->

    <!--HEADER-->
   <?php include 'include/menu.php';?>
    <!--HEADER END-->


    <!--Banner-->
    <section>
        <div class="csi-banner csi-banner-inner">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading-area">
                                <h2 class="csi-heading">
                                    About Our History
                                </h2>
                                <ul class="breadcrumb">
                                    <li><a href="index.html"><i class="icon-home6"></i>Home</a></li>
                                    <li class="active">Contact</li>
                                </ul>
                            </div>
                        </div>
                    </div><!--//.ROW-->
                </div>
                <!-- //.container -->
            </div>
        </div>
    </section>
    <!--Banner END-->


    <!--ABOUT-->
    <section>
        <div id="csi-about" class="csi-about csi-about-inner">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading">
                                <h3 class="subtitle">About Us</h3>
                                <h2 class="title">SARDAR A PURE MEAT SHOP</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-about-content">
                                <p>
                                   <b>SARDAR A PURE MEAT SHOP</b> has been a part of SARDAR FROZEN PRODUCTS since year 2002.<br>
									It was started by Mr. Harjinder Singh Sahni and Mr.Tarmeet Singh Kohli with pure intention of providing good quality meat and meat products.<br>
									SARDAR MEAT SHOP is one the India’s leading meat and poultry retail brand. All the products are safe, hygienic and without disease.<br>
									Sardar Meat Shop has its own processing plant at Sonipat where health and hygiene test are maintained while handling birds and animals. We provide fresh, frozen and ready to eat varieties of foods at our outlets.<br>
									We offer an interactive in-store experience to customers and commit operational & product excellence which apply to every outlet.
                                </p>
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--ABOUT END--> 

    <!--testimonials-->
    <section>
        <div id="csi-testimonials" class="csi-testimonials">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="csi-heading">
                                <h3 class="subtitle">What Client saying</h3>
                                <h2 class="title">Client Testimonials</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="testi-single">
                                <div class="author-area">
                                    <figure>
                                        <a class="author-img" href="#"><img src="assets/img/client1.jpg" alt="Client"/></a>
                                        <figcaption>
                                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                                        </figcaption>
                                    </figure>
                                    <div class="author-info">
                                        <h4 class="title"><a href="#">Jhone Elina</a></h4>
                                        <p class="subtitle">Head Chef</p>
                                    </div>
                                </div>
                                <div class="text">
                                    <p>
                                        Morbi imperdiet sollicitudin rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem justo, congue dapibus scelerisque at, ultricies quis mi. Pellentesque auctor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="testi-single">
                                <div class="author-area">
                                    <figure>
                                        <a class="author-img" href="#"><img src="assets/img/client1.jpg" alt="Client"/></a>
                                        <figcaption>
                                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                                        </figcaption>
                                    </figure>
                                    <div class="author-info">
                                        <h4 class="title"><a href="#">Ehone Eonathon</a></h4>
                                        <p class="subtitle">Vice President</p>
                                    </div>
                                </div>
                                <div class="text">
                                    <p>
                                        Morbi imperdiet sollicitudin rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem justo, congue dapibus scelerisque at, ultricies quis mi. Pellentesque auctor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="testi-single">
                                <div class="author-area">
                                    <figure>
                                        <a class="author-img" href="#"><img src="assets/img/client1.jpg" alt="Client"/></a>
                                        <figcaption>
                                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                                        </figcaption>
                                    </figure>
                                    <div class="author-info">
                                        <h4 class="title"><a href="#">Ehone Eonathon</a></h4>
                                        <p class="subtitle">Vice President</p>
                                    </div>
                                </div>
                                <div class="text">
                                    <p>
                                        Morbi imperdiet sollicitudin rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem justo, congue dapibus scelerisque at, ultricies quis mi. Pellentesque auctor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="testi-single">
                                <div class="author-area">
                                    <figure>
                                        <a class="author-img" href="#"><img src="assets/img/client1.jpg" alt="Client"/></a>
                                        <figcaption>
                                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                                        </figcaption>
                                    </figure>
                                    <div class="author-info">
                                        <h4 class="title"><a href="#">Ehone Eonathon</a></h4>
                                        <p class="subtitle">Vice President</p>
                                    </div>
                                </div>
                                <div class="text">
                                    <p>
                                        Morbi imperdiet sollicitudin rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem justo, congue dapibus scelerisque at, ultricies quis mi. Pellentesque auctor
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section> <!--//.testimonials-->


    <!--MILESTONE-->
    <section>
        <div id="csi-milestone-about" class="csi-milestone-about csi-milestone-about-inner">
            <div class="csi-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="csi-milestone-area">
                                <div class="csi-milestone">
                                    <div class="milestone-inner">
                                        <div class="csi-content">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="csi-counter-area">
                                                        <img src="assets/img/milestone-icon1.png" alt="milestone icon">
                                                        <div class="counter-text">
                                                            <small>Competition Winner</small>
                                                            <span class="csi-counter">500</span>
                                                        </div>
                                                    </div>
                                                </div> <!--//.COL-->
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="csi-counter-area">
                                                        <img src="assets/img/milestone-icon2.png" alt="milestone icon">
                                                        <div class="counter-text">
                                                            <small>Year of Experience</small>
                                                            <span class="csi-counter">12</span>
                                                        </div>
                                                    </div>
                                                </div> <!--//.COL-->
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="csi-counter-area">
                                                        <img src="assets/img/milestone-icon3.png" alt="milestone icon">
                                                        <div class="counter-text">
                                                            <small>Regular Clients</small>
                                                            <span class="csi-counter">7896</span>
                                                        </div>
                                                    </div>
                                                </div> <!--//.COL-->
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="csi-counter-area">
                                                        <img src="assets/img/milestone-icon4.png" alt="milestone icon">
                                                        <div class="counter-text">
                                                            <small>Skilled Chefs</small>
                                                            <span class="csi-counter">600</span>
                                                        </div>
                                                    </div>
                                                </div> <!--//.COL-->
                                            </div> <!--//.ROW-->
                                        </div> <!--//. csi CONTENT CONTENT-->
                                    </div><!--//.csi INNER-->
                                </div><!--//.Milestone End -->
                            </div>
                        </div>
                    </div>
                </div><!-- //.CONTAINER -->
            </div><!-- //.INNER -->
        </div>
    </section>
    <!--MILESTONE END-->
        
<?php include 'include/footer.php';?>
