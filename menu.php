<?php include 'include/header.php';?>

<body class="page page-template">



<!--[if lt IE 8]>

<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>

<![endif]-->



<div class="csi-container ">

    <!-- ***  ADD YOUR SITE CONTENT HERE *** -->

<?php include 'include/menu.php';?>

    <!--Banner-->

    <section>

        <div class="csi-banner csi-banner-inner">

            <div class="csi-inner">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="csi-heading-area">

                                <h2 class="csi-heading">

                                    Menu Of The Day

                                </h2>

                                <ul class="breadcrumb">

                                    <li><a href="index.html"><i class="icon-home6"></i>Home</a></li>

                                    <li class="active">Menu</li>

                                </ul>

                            </div>

                        </div>

                    </div><!--//.ROW-->

                </div>

                <!-- //.container -->

            </div>

        </div>

    </section>

    <!--Banner END-->





    <!--MENU ITEMS-->

    <section>

        <div id="csi-menu" class="csi-menu csi-menu-inner">

            <div class="csi-inner">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12">



                            <div class="csi-nav-pills-area">

                                <ul class="nav nav-pills csi-nav-pills">

                                    <li class="active"><a data-toggle="pill" href="#home"><img src="assets/img/tab-menu1.png" alt=""> Chicken</a></li>

                                    <li><a data-toggle="pill" href="#menu1"><img src="assets/img/tab-menu2.png" alt=""> Mutton</a></li>

                                    <li><a data-toggle="pill" href="#menu2"><img src="assets/img/tab-menu3.png" alt=""> Pork</a></li>

                                    <li><a data-toggle="pill" href="#menu3"><img src="assets/img/tab-menu4.png" alt=""> Sea Food</a></li>

                                    <li><a data-toggle="pill" href="#menu4"><img src="assets/img/tab-menu4.png" alt=""> Heat and Eat</a></li>

                                    <li><a data-toggle="pill" href="#menu5"><img src="assets/img/tab-menu4.png" alt=""> AnyTime Snacks</a></li>

                                </ul>
                            </div>



                            <div class="tab-content csi-tab-content">

                                <div id="home" class="tab-pane fade in active">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  

                                </div>


                                <div id="menu1" class="tab-pane fade"> 
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  

                                </div>

  
                                <div id="menu2" class="tab-pane fade">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  

                                </div>
 

                                <div id="menu3" class="tab-pane fade">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-bord">
                                            <div class="item-thumb">
                                                <img src="assets/img/chicken_nug.jpg">
                                            </div>
                                            <div class="item-head">
                                                <span>Chicken Tandoori</span>
                                            </div>
                                            <div class="item-detl">
                                                <span>₹ 20</span>
                                            </div>
                                            <div class="item-addto">
                                                <a href="#" class="atc-cart">Add to cart</a>
                                            </div>
                                        </div>
                                    </div><!--//.csi-single-tab-->  
                                    
                                </div>
 

								<div id="menu4" class="tab-pane fade">
                                        
								</div>
 

								<div id="menu5" class="tab-pane fade in">
            
								</div>

                            </div> 

                        </div>

                    </div>

                </div><!-- //.CONTAINER -->

            </div><!-- //.INNER -->

        </div>

    </section>

    <!--MENU ITEMS END-->



<?php include 'include/footer.php';?>

